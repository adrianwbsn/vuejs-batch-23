//Soal No 1
var nilai = 95

if (nilai <= 100 && nilai > 0){
    if(nilai > 85){
        console.log("Indeksnya A")
    }
    else if(nilai >= 75 && nilai < 85){
        console.log("Indeksnya B")
    }
    else if(nilai >=65 && nilai < 75){
        console.log("Indeksnya C")
    }
    else if(nilai >=55 && nilai < 65){
        console.log("Indeksnya D")
    }
    else if(nilai < 55){
        console.log("Indeksnya E")
    }
}
else{
    console.log("Score out of range")
}

console.log('-------------------------------')

//Soal No 2

var tanggal = 27
var bulan = 10
var tahun = 1998

switch(bulan){
    case 1: {console.log(tanggal + " Januari " + tahun); break;}
    case 2: {console.log(tanggal + " Februari " + tahun); break;}
    case 3: {console.log(tanggal + " Maret " + tahun); break;}
    case 4: {console.log(tanggal + " April " + tahun); break;}
    case 5: {console.log(tanggal + " Mei " + tahun); break;}
    case 6: {console.log(tanggal + " Juni " + tahun); break;}
    case 7: {console.log(tanggal + " Juli " + tahun); break;}
    case 8: {console.log(tanggal + " Agustus " + tahun); break;}
    case 9: {console.log(tanggal + " September " + tahun); break;}
    case 10: {console.log(tanggal + " Oktober " + tahun); break;}
    case 11: {console.log(tanggal + " November " + tahun); break;}
    case 12: {console.log(tanggal + " Desember " + tahun); break;}
    default: {console.log("Out of Range")}
}

console.log('-------------------------------')

//Soal No 3
var n = 7
var symbols = ""

while(n > 0){
    symbols += "#"
    n--
    console.log(symbols)
}

console.log('-------------------------------')

//Soal No 4
var daftarbuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"]

for(var deret = 5; deret > 0; deret--){
    console.log(daftarbuah[4])
    for(deret = 4; deret > 0;){
        console.log(daftarbuah[0])
        for(deret = 3; deret > 0;){
            console.log(daftarbuah[2])
            for(deret = 2; deret > 0;){
                console.log(daftarbuah[3])
                for(deret = 1; deret > 0;deret--){
                    console.log(daftarbuah[1])
                }
            }
        }
    }
}