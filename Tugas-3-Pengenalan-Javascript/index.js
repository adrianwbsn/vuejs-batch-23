//Soal 1

var pertama = "saya sangat senang hari ini"
var kedua = "belajar javascript itu keren"

// saya senang belajar JAVASCRIPT

var kata1 = pertama.substr(0, 5)
var katasn = pertama.substring(12)
var kata2 = katasn.substr(0, 7)
var kata3 = kedua.substr(0, 8)
var katajvdel = kedua.substring(7)
var katajv = katajvdel.substr(0,11)
var katajvtr = katajv.trim()
var kata4 = katajvtr.toUpperCase()

console.log(kata1.concat(kata2).concat(kata3).concat(kata4))

//Soal 2

var katapertama = 10
var katakedua = 2
var kataketiga = 4
var katakeempat = 6

console.log((katapertama % katakedua) + (kataketiga * katakeempat))

//Soal 3

var kalimat = "wah javascript itu keren sekali"
var kataPertama = kalimat.substring(0, 3)
var kataKedua = kalimat.substring(4, 14)
var kataKetiga = kalimat.substring(15, 18)
var kataKeempat = kalimat.substring(19, 24)
var kataKelima = kalimat.substring(25, 31)

console.log('Kata Pertama: ' + kataPertama) 
console.log('Kata Kedua: ' + kataKedua) 
console.log('Kata Ketiga: ' + kataKetiga) 
console.log('Kata Keempat: ' + kataKeempat)
console.log('Kata Kelima: ' + kataKelima)